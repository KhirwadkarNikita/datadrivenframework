package ExcelPractice;

import org.testng.annotations.Test;

import appModules.SignIn_Action;
import pageObjects.SinOn_POM;
import utility.Constant;
import utility.ExcelUtils;

import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;

public class Apache_POI_TC {
	public WebDriver driver;
	SinOn_POM signin;
  @Test
  public void DemoToursSignIn() throws Exception {
	  
	  ExcelUtils.setExcelFile(Constant.Path_TestData+Constant.File_TestData, "Sheet1");
	  driver.get(Constant.URL);
	  SignIn_Action.Execute(driver);
	  ExcelUtils.setCellData("Pass", 1, 3);
  }
  @BeforeTest
  public void beforeTest() {
	  System.setProperty("webdriver.chrome.driver", "D:\\Selenium Setup\\drivers\\chrome driver\\chromedriver.exe");
	  driver = new ChromeDriver();
	  signin = PageFactory.initElements(driver, SinOn_POM.class);
  }

  @AfterTest
  public void afterTest() {
	  driver.quit();
  }

}
