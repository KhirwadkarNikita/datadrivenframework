package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

//import org.testng.annotations.Test;

public class SinOn_POM {
	
	public WebDriver driver;
	
	@FindBy(how = How.NAME, using = "userName")
	public static WebElement txtbx_UserName;
	
	@FindBy(how = How.NAME, using="password")
	public static WebElement txtbx_Password;
	
	@FindBy(how = How.NAME, using = "login")
	public static WebElement btn_SignIn;
	
	public static void Login_Action(String sUserName, String sPassword){
	txtbx_UserName.sendKeys(sUserName);
	txtbx_Password.sendKeys(sPassword);
	btn_SignIn.click();
	}
  
  
}
