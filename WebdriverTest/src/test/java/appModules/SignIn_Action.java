package appModules;

import org.openqa.selenium.WebDriver;

import pageObjects.SinOn_POM;
import utility.ExcelUtils;

public class SignIn_Action {
	public WebDriver driver;
	public static void Execute(WebDriver driver) throws Exception{
		String uname = ExcelUtils.getCellData(1,1);
		String pwd = ExcelUtils.getCellData(1,2);
		
		SinOn_POM.Login_Action(uname, pwd);
		
	}

}
